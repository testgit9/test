
# USER GUIDE

## FOR SERVER

1. start the program with command line ./server  
2. you can wait for the client or add food by yourself  
2.1 Clients send the request:  
- You will see message like this:s    
    - If there is a new client with food's name:
>Message received: 'request a 1'  
>"1" request "a"  
>Khach hang co ID: 1 da dat mon a vui long cho trong giay lat !  
>Danh sach khach hang:  
>STT: 0 ID khach: 1 
    
-If there is a new order from same client:  
>Message received: 'request b 1'  
>"1" request "b"  
>Add ID: 1 b  
>Khach hang co ID: 1 da dat **THEM** mon b vui long cho !  
>Danh sach khach hang:  
>STT: 0 ID khach: 1
    
- Serve the clients:
    - Write exactly the folowwing command: _serve_
    - You will see the message like this:

>Khach hang ke tiep ID: 1 mon a  
>Danh sach khach hang:  
>STT: 0 ID khach: 1  
>id khach: 1 mon con lai: b  
>id khach: 1 dem cnt: 1  

It will show you the next client should be serve and how many they were served  
  
The diagram:
```mermaid
sequenceDiagram
autonumber
ClientX->>Server: new order
Note left of ClientX: command: request <food_char>
Note right of Server: get mess: '<id>' request '<food_char>'
Server->>Server: insert and check database
ClientY->>Server: new order
Note right of Server: get mess: '<id>' request '<food_char>'
Server->>Server: insert and check database
Server-->>ClientX: serve
Note right of Server: command: serve
Note left of ClientX: get mess: Don mon '<food_c>'
Server -->>ClientY: serve
Note left of Server: command: serve
Note right of ClientY: get mess: Don mon '<food_c>'



```

2.2 Add by yourself:  
- Add new customer: _add <id_client> <food_char_name> <isVip> <alternative_name_call>_  
- Serve the clients:  
    - Write exactly the folowwing command: _serve_

    
## FOR SERVER
1. Run code with the ID: /cusid -id <yourID>
2. Type the command: request food_char_name
3. Wait for be served:  
    You will get a message like this:  
    Message received from server: Don mon "a"
    



  
